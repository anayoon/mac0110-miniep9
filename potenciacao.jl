using LinearAlgebra

function multiplica(a, b)
	dima = size(a)
	dimb = size(b)
	if dima[2] != dimb[1]
		return -1	
	end
	c = zeros(dima[1], dimb[2])
	for i in 1:dima[1]
		for j in 1:dimb[2]
			BigFloat(c[i,j])
			for k in 1:dima[2]
			c[i, j] = c[i, j] + a[i, k] * b[k, j]
			end
		end
	end
	return c
end

function matrix_pot(M,p)
	aux=p
	if p==0
		return 1
	end
	x=M
	while aux>1
		x=multiplica(x,M)
		aux-=1
	end
	return x
end

using Test

function test_pot()
	@test matrix_pot([1 2 ; 3 4], 1)==[1 2;3 4]
	@test matrix_pot([1 2 ; 3 4], 2)==[7.0 10.0;15.0 22.0]
	@test matrix_pot([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7)==[5.6428432e8 4.70888932e8 3.23583236e8 3.51858636e8; 8.31242352e8 6.93529366e8 4.76618654e8 5.18192434e8; 5.77003992e8 4.81472568e8 3.30793288e8 3.59676984e8; 7.99037372e8 6.66708057e8 4.58121425e8 4.98126827e8]
	println("Final dos testes da funcao matrix_pot")
end

test_pot()

function matrix_pot_by_squaring(M, p)
	identidade=[]
	a=size(M)
	I = Matrix(LinearAlgebra.I, a[1], a[2])
	if  p<0
		return matrix_pot_by_squaring(1/M, -p)
	elseif p==0
		return I
	elseif  p==1
		return M
	elseif  p>0 && p%2==0
		return matrix_pot_by_squaring(multiplica(M, M), p/2)
	else
		return multiplica(M,matrix_pot_by_squaring(multiplica(M, M), (p-1)/2))
	end
end

function test_pot_by_squaring()
	@test matrix_pot_by_squaring([1 2 ; 3 4], 1)==[1 2;3 4]
	@test matrix_pot_by_squaring([1 2 ; 3 4], 2)==[7.0 10.0;15.0 22.0]
	@test matrix_pot_by_squaring([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7)==[5.6428432e8 4.70888932e8 3.23583236e8 3.51858636e8; 8.31242352e8 6.93529366e8 4.76618654e8 5.18192434e8; 5.77003992e8 4.81472568e8 3.30793288e8 3.59676984e8; 7.99037372e8 6.66708057e8 4.58121425e8 4.98126827e8]
	println("Final dos testes da funcao matrix_pot_by_squaring")
end

test_pot_by_squaring()

function compare_times()
	M = Matrix(LinearAlgebra.I, 30, 30)
	#println("tempos de matrix_pot: ")
	@time matrix_pot(M, 10)
	#println("tempos de matrix_pot_by_squaring: ")
	@time matrix_pot_by_squaring(M, 10)
	#println("tempos de matrix_pot: ")
	@time matrix_pot([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7)
	#println("tempos de matrix_pot_by_squaring: ")
	@time matrix_pot_by_squaring([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7)
	#println("tempos de matrix_pot: ")
	@time matrix_pot([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 1000)
	#println("tempos de matrix_pot_by_squaring: ")
	@time matrix_pot_by_squaring([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 1000)
end
compare_times()

